import numpy as np
import re
import cv2
import operator
import json
import time
import os


class ROI:
    def __init__(self, group, percentage, coordinates, center, slide_index):
        self.group = group
        self.percentage = percentage
        self.coordinates = coordinates
        self.center = center
        self.slide_index = slide_index


class Slide:
    def __init__(self, name):
        self.name = name
        self.data = None
        self.groups = {}
        self.all = []


class Whole_set():
    def __init__(self, space_size, center_size, size_expend):
        self.images = {}

        self.space_size = space_size
        self.center_size = center_size
        self.size_expend = size_expend

        self.operations = {
            "th": self.threshold,
            "at_m": self.at_mean,
            "at_g": self.at_gaussian,
            "blur": self.blur,
            "erode": self.erode,
            "dilate": self.dilate,
            "btw": self.btw_and,
            "edge": self.edge,
            "equalization": self.equalization,
            "t_hat": self.top_hat,
            "b_hat": self.black_hat,
            "otsu_t": self.otsu_tresholding,
            'ccvs': self.ccvs,
            'ccvs_l': self.ccvs_largest,
            'co': self.conture,
            'opening': self.opening,
            'closing': self.closing,
            'fit_ecl': self.fit_elipse,
            'inverse': self.inverse
        }

        self.ops = {}

        self.file_group = {
            'nucleus': 0,
            'white': 0,
            'black': 0,
            'onion': 0,
            'zebra': 0
        }

        self.colors = {
            'white': (255, 255, 255),
            'black': (0, 0, 0),
            'nucleus': (0, 127, 204),
            'onion': (51, 0.0, 255),
            'zebra': (127, 127, 25)
        }

        self.read_all()

    def inverse(self, row):
        self.current = (255 - self.current)

    def read_all(self):
        index = -1
        with open('bioNucleusResult.txt', 'r') as file:
            for line in file:
                if 'images' in line:
                    index += 1
                    result = re.search('/(.*):', line)
                    imgName = result.group(1)
                    self.images[index] = Slide(imgName)
                elif index >= 0:
                    result = line.split()
                    if len(result) == 10 and result[0][:-1] != 'worm':
                        coords = [
                            (i > 0) * i for i in map(int, [result[3], result[5], result[7], result[9][:-1]])]
                        x, y, w, h = coords
                        center = (x + w / 2, y + h / 2)
                        self.images[index].all.append(
                            ROI(result[0][:-1], result[1], coords, center, index))
        self.save_all()

    def set_block_size(self, bs):
        if bs <= 2:
            bs = 3
        if bs % 2 != 1:
            bs += 1
        return bs

    def threshold(self, params):
        threshold = int(params[0])
        ret, self.current = cv2.threshold(
            self.current, threshold, 255, cv2.THRESH_BINARY)

    def at_mean(self, params):
        blockSize = self.set_block_size(int(params[0]))
        C = int(params[1])
        self.current = cv2.adaptiveThreshold(
            self.current, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, blockSize, C)

    def at_gaussian(self, params):
        blockSize = self.set_block_size(int(params[0]))
        C = int(params[1])
        self.current = cv2.adaptiveThreshold(
            self.current, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, blockSize, C)

    def conture(self, params):
        self.current = np.uint8(self.current * 255)
        contours, hierarchy = cv2.findContours(
            self.current, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        for contour in contours:
            cv2.drawContours(self.current, contour, -1,
                             (255, 255, 255), cv2.FILLED)

    def blur(self, params):
        blur = int(params[0])
        if blur % 2 != 1:
            blur += 1
        self.current = cv2.medianBlur(self.current, blur)

    def opening(self, params):
        size = int(params[0])
        iterations = int(params[1])
        self.current = cv2.erode(self.current, np.ones(
            (size, size), np.uint8), iterations=iterations)
        self.current = cv2.dilate(self.current, np.ones(
            (size, size), np.uint8), iterations=iterations)

    def closing(self, params):
        size = int(params[0])
        iterations = int(params[1])
        self.current = cv2.dilate(self.current, np.ones(
            (size, size), np.uint8), iterations=iterations)
        self.current = cv2.erode(self.current, np.ones(
            (size, size), np.uint8), iterations=iterations)

    def erode(self, params):
        size = int(params[0])
        iterations = int(params[1])
        self.current = cv2.erode(self.current, np.ones(
            (size, size), np.uint8), iterations=iterations)

    def dilate(self, params):
        size = int(params[0])
        iterations = int(params[1])
        self.current = cv2.dilate(self.current, np.ones(
            (size, size), np.uint8), iterations=iterations)

    def btw_and(self, params):
        if params[0] == 'True':
            self.current = cv2.bitwise_and(
                self.original, self.original, mask=cv2.bitwise_not(self.current))
        else:
            self.current = cv2.bitwise_and(
                self.original, self.original, mask=self.current)

    def edge(self, params):
        self.current = cv2.Canny(self.current, 100, 100)

    def equalization(self, params):
        self.current = cv2.equalizeHist(self.current)

    def top_hat(self, params):
        size = int(params[2])
        self.current = cv2.morphologyEx(
            self.current, cv2.MORPH_TOPHAT, np.ones((size, size), np.uint8))

    def black_hat(self, params):
        size = int(params[2])
        self.current = cv2.morphologyEx(
            self.current, cv2.MORPH_BLACKHAT, np.ones((size, size), np.uint8))

    def ccvs(self, params):
        num_labels, labels, stats, centroids = cv2.connectedComponentsWithStats(
            self.current, 4, cv2.CV_32S)
        for i in range(num_labels):
            if stats[i][4] < int(params[2]) and stats[i][4] > int(params[3]):
                cv2.rectangle(self.current, (stats[i][0], stats[i][1]), (
                    stats[i][0] + stats[i][2], stats[i][1] + stats[i][3]), (0, 255, 0), 1)

    def fit_elipse(self, row):
        try:
            im, contours, hierarchy = cv2.findContours(
                self.current, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            contour = max(contours, key=cv2.contourArea)
            self.current = np.zeros(self.current.shape)
            cv2.ellipse(self.current, cv2.fitEllipse(contour), (255, 255, 255))
        except Exception:
            pass

    def ccvs_largest(self, params):
        num_labels, labels, stats, centroids = cv2.connectedComponentsWithStats(
            self.current, 4, cv2.CV_32S)

        sizes = stats[:, -1]
        max_label = 1
        max_size = sizes[1]
        for i in range(2, num_labels):
            if sizes[i] > max_size:
                max_label = i
                max_size = sizes[i]

        self.current = np.zeros(labels.shape, dtype=np.uint8)
        self.current[labels == max_label] = 255

    def otsu_tresholding(self, params):
        ret, self.current = cv2.threshold(
            self.current, 0, 255, cv2.THRESH_OTSU)

    def find_object(self, index, roi):
        rk = 0
        group = {}
        without = self.space_size
        obj = []
        max_size = self.center_size
        for i in range(index, len(self.images)):
            for j in range(len(self.images[i].all)):
                if abs(roi.center[0] - self.images[i].all[j].center[0]) < max_size and abs(roi.center[1] - self.images[i].all[j].center[1]) < max_size:
                    max_size = self.center_size
                    roi = self.images[i].all.pop(j)
                    obj.append(roi)
                    without = self.space_size
                    if roi.group not in group:
                        group[roi.group] = 0
                    group[roi.group] += int(roi.percentage[:-1])
                    break
            else:
                rk += 1
                obj.append(None)
                without -= 1
                max_size += self.size_expend
                if without == 0:
                    break
        group = max(group.items(), key=operator.itemgetter(1))[0]
        return obj, group

    def estimate(self, prew_one, next_one, size, index):
        coords = []
        for i in range(4):
            coords.append(
                int(prew_one[i] - ((prew_one[i] - next_one[i]) / size)))
        center = (coords[0] + coords[2] / 2, coords[1] + coords[3] / 2)
        return ROI(None, '0%', coords, center, index + 1)

    def save_ply(self, obj, ind, group):
        counter = 0
        coords = ''
        for i in range(len(obj)):
            if not obj[i]:
                size = 1
                while not obj[i + size]:
                    size += 1
                obj[i] = self.estimate(
                    obj[i - 1].coordinates, obj[i + size].coordinates, size + 1, obj[i + size].slide_index)
            else:
                x, y, w, h = obj[i].coordinates
                if self.images[ind + i].data is None:
                    self.images[ind + i].data = cv2.imread(
                        'images/' + self.images[ind + i].name, cv2.IMREAD_GRAYSCALE)
                self.current = self.images[ind + i].data[y:y + h, x:x + w]

                for key in self.ops[group].keys():
                    self.operations[key](self.ops[group][key])

                indices = np.where(self.current == [255])
                for j in range(len(indices[0])):
                    counter += 1
                    coords += str(float(indices[1][j] + x) / 100) + " " + str(
                        float(indices[0][j] + y) / 100) + " " + str(float(ind + i) / 100) + ' 0 0 0\n'
        header = 'ply\nformat ascii 1.0\nelement vertex ' + \
            str(counter) + '\nproperty float x\nproperty float y\nproperty float z\nelement face 0\nproperty list uchar int vertex_indices\nend_header\n'

        with open(f'{group}/{group}_{self.file_group[group]}.ply', 'w') as file:
            file.write(header + coords)

    def clear_folder(self, path):
        for root, dirs, files in os.walk(path):
            for f in files:
                os.unlink(os.path.join(root, f))

    def save_all(self, n=10):
        with open('ops_final.json') as file:
            self.ops = json.load(file)

        for k in self.file_group:
            self.clear_folder(k)

        counter = 0
        km = 0
        for i in self.images:
            while len(self.images[i].all) > 0:
                roi = self.images[i].all[0]
                obj, group = self.find_object(i, roi)
                if len(obj) >= n + 15:
                    counter += 1
                    while obj[-1] is None:
                        del obj[-1]
                    self.file_group[group] += 1
                    self.save_ply(obj, i, group)
                    
            self.images[i].data = None


st = time.time()
Whole_set(13, 7.7, 1.07)
print(time.time() - st)
