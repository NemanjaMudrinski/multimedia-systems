import bpy
from math import radians

bl_info = {
    "name": "Bio slider",
    "blender": (2, 80, 0),
    "category": "Object"
}


class BioSlider(bpy.types.Operator):
    """Bio slider"""
    bl_idname = "bio.create"
    bl_label = "Bio slider"
    bl_options = {'REGISTER', 'UNDO'}

    sliceNum: bpy.props.IntProperty(name="Slide number", min=0, max=1777)
    slices = []

    with open(r'C:\Users\WDeath\Downloads\Projekat\names.txt', 'r') as file:
        for line in file:
            slices.append(line.strip())

    def execute(self, context):
        bpy.ops.import_image.to_plane(files=[{"name": self.slices[self.sliceNum],
                                              "name":self.slices[self.sliceNum]}],
                                      directory="C:\\Users\\WDeath\\Downloads\\Projekat\\images_labeled\\",
                                      align_axis='Z+', relative=False)

        bpy.ops.transform.translate(value=(10.1, 4.23, self.sliceNum / 100),
                                    orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)),
                                    orient_matrix_type='GLOBAL', mirror=True, use_proportional_edit=False,
                                    proportional_edit_falloff='SMOOTH', proportional_size=1,
                                    use_proportional_connected=False,
                                    use_proportional_projected=False)

        bpy.ops.transform.resize(value=(8.450, 8.450, 1),
                                 orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)),
                                 orient_matrix_type='GLOBAL', mirror=True, use_proportional_edit=False,
                                 proportional_edit_falloff='SMOOTH', proportional_size=1,
                                 use_proportional_connected=False, use_proportional_projected=False)

        bpy.ops.transform.rotate(value=radians(180), orient_axis='Y', orient_type='GLOBAL',
                                 orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL',
                                 constraint_axis=(False, True, False), mirror=True, use_proportional_edit=False,
                                 proportional_edit_falloff='SMOOTH', proportional_size=1,
                                 use_proportional_connected=False, use_proportional_projected=False)

        bpy.ops.transform.rotate(value=radians(180), orient_axis='Z', orient_type='GLOBAL',
                                 orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL',
                                 constraint_axis=(False, False, True), mirror=True, use_proportional_edit=False,
                                 proportional_edit_falloff='SMOOTH', proportional_size=1,
                                 use_proportional_connected=False, use_proportional_projected=False)

        return {'FINISHED'}


addon_keymaps = []


def menu_func(self, context):
    self.layout.operator(BioSlider.bl_idname)


def register_keys():
    wm = bpy.context.window_manager
    if wm.keyconfigs.addon:
        km = wm.keyconfigs.addon.keymaps.new(name="Object Mode", space_type="EMPTY")
        kmi = km.keymap_items.new(BioSlider.bl_idname, 'F', 'PRESS', ctrl=True, shift=True)
        addon_keymaps.append((km, kmi))


def register():
    bpy.utils.register_class(BioSlider)
    bpy.types.VIEW3D_MT_object.append(menu_func)

    register_keys()


def unregister_keymaps():
    for km, kmi in addon_keymaps:
        km.keymap_items.remove(kmi)
    addon_keymaps.clear()


def unregister():
    unregister_keymaps()

    bpy.utils.unregister_class(BioSlider)
    bpy.types.VIEW3D_MT_object.remove(menu_func)


if __name__ == "__main__":
    register()
