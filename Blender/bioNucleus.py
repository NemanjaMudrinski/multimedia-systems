import bpy
import os
import time

bl_info = {
    "name": "Bio nucleus loader",
    "blender": (2, 80, 0),
    "category": "Object"
}


class BioNucleus(bpy.types.Operator):
    """Bio nucleus"""
    bl_idname = "bio_nucleus.create"
    bl_label = "Bio nucleus"
    bl_options = {'REGISTER', 'UNDO'}

    colors = {
        'white': (1, 1, 1, 1),
        'black': (0, 0, 0, 1),
        'nucleus': (0.5, 0.5, 0.1, 1),
        'onion': (0.2, 0.0, 1, 1),
        'zebra': (0, 0.5, 0.8, 1)
    }

    filepath = bpy.props.StringProperty(subtype="FILE_PATH")
    render_mesh = bpy.props.BoolProperty(name="Render mesh", default=False)
    c_h = bpy.props.BoolProperty(name="Convex hull", default=True)
    cubeNum = bpy.props.IntProperty(name="Number of cubes in xyz", default=100, min=1, max=1000)

    def execute(self, context):
        """Select file/folder."""
        for k, v in self.colors.items():
            if bpy.data.materials.get(k) is None:
                bpy.data.materials.new(k)
            bpy.data.materials[k].diffuse_color = v

        if self.render_mesh:
            filename, extension = os.path.splitext(self.filepath)
            if filename:
                bpy.ops.object.select_all(action='DESELECT')
                if not extension:
                    self.create_from_ply(self.filepath, filename)
                elif extension == '.xyz':
                    self.create_cube(self.filepath)
        self.render_mesh = False
        return {'FINISHED'}

    def create_from_ply(self, filepath, filename):
        st = time.time()
        class_type = os.path.basename(filename)
        if not class_type:
            class_type = os.path.basename(filename[:-1])
        for root, dirs, files in os.walk(filepath, topdown=False):
            for name in files:
                if os.path.join(root, name)[-4:] == ".ply":
                    bpy.ops.import_mesh.ply(filepath=os.path.join(root, name))
        if len(bpy.context.selected_objects) > 0:
            self.create_collection(class_type)
            bpy.ops.object.editmode_toggle()
            if class_type in self.colors:
                for obj in bpy.context.selected_objects:
                    bpy.context.view_layer.objects.active = obj
                    bpy.context.object.data.materials.append(bpy.data.materials[class_type])
            if self.c_h:
                bpy.ops.mesh.convex_hull()
            bpy.ops.object.editmode_toggle()

        print(time.time() - st)

    def create_collection(self, coll_name):
        if not bpy.data.collections.get(coll_name):
            bpy.context.scene.collection.children.link(bpy.data.collections.new(name=coll_name))
        for index, col in enumerate(bpy.context.scene.collection.children):
            if col.name == coll_name:
                bpy.ops.object.move_to_collection(collection_index=index + 1)
                break

    def create_cube(self, path):
        with open(path) as f:
            kocke = []
            for line in f:
                kocke.append(list(map(float, line.split())))
            if self.cubeNum > len(kocke):
                self.cubeNum = len(kocke)
            everyNth = len(kocke) // self.cubeNum
            for i in range(self.cubeNum):
                xyz = kocke[i * everyNth]
                bpy.ops.mesh.primitive_cube_add(size=4, enter_editmode=True, location=(xyz[0], xyz[1], xyz[2]))
            bpy.ops.object.editmode_toggle()
        obj_name = os.path.basename(path)
        if not obj_name:
            obj_name = os.path.basename(path[:-1])
        bpy.context.active_object.name = obj_name


addon_keymaps = []


def menu_func(self, context):
    self.layout.operator(BioNucleus.bl_idname)


def register_keys():
    wm = bpy.context.window_manager
    if wm.keyconfigs.addon:
        km = wm.keyconfigs.addon.keymaps.new(name="Object Mode", space_type="EMPTY")
        kmi = km.keymap_items.new(BioNucleus.bl_idname, 'L', 'PRESS', ctrl=True, shift=True)
        addon_keymaps.append((km, kmi))


def register():
    bpy.utils.register_class(BioNucleus)
    bpy.types.VIEW3D_MT_object.append(menu_func)

    register_keys()


def unregister_keymaps():
    for km, kmi in addon_keymaps:
        km.keymap_items.remove(kmi)
    addon_keymaps.clear()


def unregister():
    unregister_keymaps()

    bpy.utils.unregister_class(BioNucleus)
    bpy.types.VIEW3D_MT_object.remove(menu_func)


if __name__ == "__main__":
    register()
